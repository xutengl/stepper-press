#include "stepperPress.h"

bool StepperPress::isValid()
{
  if (cmd_len + 1 != PROTOCOL_LEN)
    return false;

  if (rxBuf[0] != StepperPress::_SOF)
    return false;

  return true;
}

void StepperPress::decode()
{
  ena = bool(rxBuf[1] & 1 << 4);
  dir = bool(rxBuf[1] & 1 << 0);
  frequency = uint16_t(rxBuf[2] << 8 | rxBuf[3]);
  pulseCount = uint16_t(rxBuf[4] << 8 | rxBuf[5]);
  memset(rxBuf, 0, RX_BUF_SIZE);
}

void StepperPress::execute()
{
  digitalWrite(_en_pin, ena);
  digitalWrite(_dir_pin, dir);
  pulseGen(_pul_pin, frequency, pulseCount);
}

int StepperPress::getExecutionTime()
{
  int duration_ms = pulseCount / frequency * 1000;
  return duration_ms;
}

void StepperPress::readCmd()
{
  cmd_len = _Serial->readBytesUntil(_EOF, rxBuf, PROTOCOL_LEN * 2);
  Serial.flush();
}

char *StepperPress::showDecodedMsg()
{
  static char msg[32];
  sprintf(msg, "\ndecode result:\nena: %d, dir: %d, freq: %d, pcnt: %d\n", ena, dir, frequency, pulseCount);
  _Serial->println(msg);
  return msg;
}

char *StepperPress::showRxBuffer()
{
  static char msg[64];
  sprintf(msg, "received %d bytes: ", cmd_len);
  int pos = strlen(msg);

  for (int i = 0; i < cmd_len; i++)
  {
    pos += sprintf(&msg[pos], "%02X ", rxBuf[i]);
  }
  _Serial->println(msg);
  return msg;
}

void StepperPress::init(unsigned long baudrate)
{
  pinMode(_pul_pin, OUTPUT);
  pinMode(_dir_pin, OUTPUT);
  pinMode(_en_pin, OUTPUT);
  _Serial->begin(baudrate);
  _Serial->println("setup finished");
}

void StepperPress::run()
{
  if (_Serial->available())
  {
    readCmd();
    showRxBuffer();

    if (isValid())
    {
      decode();

      showDecodedMsg();

      _Serial->println("executing command, serial communication paused...");
      execute();
      delay(getExecutionTime());
      _Serial->println("cmd execution finished, ready to receive new cmd...");
    }
    else
    {
      _Serial->println("invalid cmd");
    }
  }
}