
/*
  this code runs a stepper motor with given frequency and pulse count

  We must modify the folloing function inside “tone.cpp” file to run this code correctly.

          void tone(uint8_t _pin, unsigned int frequency, unsigned long duration)

  we will change the last variable from duration to pulse_count:

          void tone(uint8_t _pin, unsigned int frequency, unsigned long pulse_count)

  and we replace the following code insode the function with our code to Calculate the toggle count from pulse count

  original code:

          // Calculate the toggle count
          if (duration > 0)
          {
            toggle_count = 2 * frequency * duration / 1000;
          }
          else
          {
            toggle_count = -1;
          }

    our code:

            // Calculate the toggle count
            if(pulse_count>0)
            {
              toggle_count = pulse_count * 2;
            }else
            {
              toggle_count = -1;
            }

    that's it. Enjoy~
*/

#pragma once

#include <inttypes.h>
#include <Arduino.h>

#define pulseGen tone

class StepperPress
{
private:
  HardwareSerial *const _Serial;
  const uint8_t _pul_pin, _dir_pin, _en_pin;
  const static uint8_t PROTOCOL_LEN = 7;
  const static uint8_t RX_BUF_SIZE = 64;
  const static uint8_t _SOF = (0xAA); // start of frame
  const static uint8_t _EOF = (0x0A); // end of frame

  uint16_t frequency;
  uint16_t pulseCount;
  bool dir;
  bool ena;

  uint8_t cmd_len = 0;

public:
  uint8_t rxBuf[RX_BUF_SIZE];

  StepperPress(HardwareSerial *Serial, uint8_t pul_pin, uint8_t dir_pin, uint8_t en_pin) : _Serial(Serial),
                                                                                           _pul_pin(pul_pin),
                                                                                           _dir_pin(dir_pin),
                                                                                           _en_pin(en_pin)
  {
  }

  void init(unsigned long baudrate);
  void run();

  void readCmd();
  bool isValid();
  void decode();
  void execute();
  int getExecutionTime();

  char *showDecodedMsg();
  char *showRxBuffer();
};
