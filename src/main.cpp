#include <Arduino.h>
#include "stepperPress.h"

#define PUL_PIN (2)
#define DIR_PIN (3)
#define EN_PIN (4)

StepperPress myStepper(&Serial, PUL_PIN, DIR_PIN, EN_PIN);

void setup() {
  myStepper.init(115200);
}

void loop() {
  myStepper.run();
  delay(10);
}

